# BAMBU Base Service

Base service configuration for all microservices

### Prerequisites
We will be using the latest LTS version of Node.js, which at the time of writing, is v8.9.4.

**Node.js** - download at [nodejs.org](https://nodejs.org/en/) or install via [Node Version Manager](https://github.com/creationix/nvm), and

**npm** - v5.6.0 included with your Node.js installation.

Install loopback cli

```
  npm install -g loopback-cli
```
This installs the *lb* command-line tool for scaffolding and modifying LoopBack applications

## Initialization

```Shell
npm install
```

Install all project dependencies

### Development

```Shell
npm start
```

Starts the development server and makes your application accessible at
`localhost:3000`. 

### Creating Restful microservies using [Loopback Generators](http://loopback.io/doc/en/lb3/Command-line-tools.html)

Create datasource

```
lb datasource
```

Create a model

```
lb model
```
Model should be server only, Don't select common

*Follow the succeeding steps on the cli*


## Unit testing

```Shell
npm test
```

Run the unit test written in [mocha](https://mochajs.org/) and [chai](http://chaijs.com/)

## Linting

```Shell
npm run lint
```

Lints your JavaScript using [Airbnb style guide](https://github.com/airbnb/javascript)

