const Root = require('../root');
const { expect } = require('chai');

/* eslint-disable no-undef */
describe('root module', () => {
  it('should export a function', () => {
    expect(Root).to.be.a('function');
  });
});
